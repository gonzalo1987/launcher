<?php

namespace App\Controller;

use App\Entity\ComponentType;
use App\Form\ComponentTypeType;
use App\Repository\ComponentTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/type/component")
 */
class ComponentTypeController extends AbstractController
{
    /**
     * @Route("/", name="component_type_index", methods={"GET"})
     *
     * @param ComponentTypeRepository $componentTypeRepository
     *
     * @return Response
     */
    public function index(ComponentTypeRepository $componentTypeRepository): Response
    {
        return $this->render('component_type/index.html.twig', [
            'component_types' => $componentTypeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="component_type_new", methods={"GET","POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function new(Request $request): Response
    {
        $componentType = new ComponentType();
        $form = $this->createForm(ComponentTypeType::class, $componentType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($componentType);
            $entityManager->flush();

            return $this->redirectToRoute('component_type_index');
        }

        return $this->render('component_type/new.html.twig', [
            'component_type' => $componentType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="component_type_show", methods={"GET"})
     *
     * @param ComponentType $componentType
     *
     * @return Response
     */
    public function show(ComponentType $componentType): Response
    {
        return $this->render('component_type/show.html.twig', [
            'component_type' => $componentType,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="component_type_edit", methods={"GET","POST"})
     *
     * @param Request $request
     * @param ComponentType $componentType
     *
     * @return Response
     */
    public function edit(Request $request, ComponentType $componentType): Response
    {
        $form = $this->createForm(ComponentTypeType::class, $componentType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('component_type_index');
        }

        return $this->render('component_type/edit.html.twig', [
            'component_type' => $componentType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="component_type_delete", methods={"DELETE"})
     *
     * @param Request $request
     * @param ComponentType $componentType
     *
     * @return Response
     */
    public function delete(Request $request, ComponentType $componentType): Response
    {
        if ($this->isCsrfTokenValid('delete'.$componentType->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($componentType);
            $entityManager->flush();
        }

        return $this->redirectToRoute('component_type_index');
    }
}
