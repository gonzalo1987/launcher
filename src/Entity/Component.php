<?php


namespace App\Entity;


use App\Entity\User\User;
use App\Model\AbstractTimetrackeableEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Component
 *
 * @ORM\Table(name="component")
 * @ORM\Entity(repositoryClass="App\Repository\ComponentRepository")
 * @UniqueEntity(
 *      fields={"project", "position"},
 *      errorPath="phase",
 *      message="The position exists for this project."
 * )
 */
class Component extends AbstractTimetrackeableEntity
{
    /**
     * Component constructor.
     *
     * @param Block $block
     * @param ComponentType $type
     */
    public function __construct(Block $block, ComponentType $type)
    {
        parent::__construct();

        $this->setBlock($block);
        $this->setType($type);
        $this->setValid(true);
    }

    /**
     * @var Block
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Block")
     * @ORM\JoinColumn(name="step", referencedColumnName="id")
     */
    private $block;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var ComponentType
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\ComponentType")
     * @ORM\JoinColumn(name="type", referencedColumnName="id")
     */
    private $type;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User")
     * @ORM\JoinColumn(name="reviewed_by", referencedColumnName="id", nullable=true)
     */
    private $reviewedBy;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @var Block
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Block")
     * @ORM\JoinColumn(name="parent", referencedColumnName="id", nullable=true)
     */
    private $parent;

    /**
     * @var bool
     *
     * @ORM\Column(name="valid", type="boolean")
     */
    private $valid;

    /**
     * @return Block
     */
    public function getBlock(): Block
    {
        return $this->block;
    }

    /**
     * @param Block $block
     *
     * @return $this
     */
    public function setBlock(Block $block): self
    {
        $this->block = $block;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return ComponentType
     */
    public function getType(): ComponentType
    {
        return $this->type;
    }

    /**
     * @param ComponentType $type
     *
     * @return $this
     */
    public function setType(ComponentType $type): self
    {
        $this->name = $type;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getReviewedBy(): ?User
    {
        return $this->reviewedBy;
    }

    /**
     * @param User|null $reviewer
     *
     * @return $this
     */
    public function setReviewedBy(?User $reviewer): self
    {
        $this->reviewedBy = $reviewer;

        return $this;
    }

    /**
     * @return int
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int $position
     *
     * @return $this
     */
    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return Block|null
     */
    public function getParent(): ?Block
    {
        return $this->parent;
    }

    /**
     * @param Block $parent
     *
     * @return $this
     */
    public function setParent(Block $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return bool
     */
    public function getValid(): bool
    {
        return $this->valid;
    }

    /**
     * @param bool $valid
     *
     * @return $this
     */
    public function setValid(bool $valid): self
    {
        $this->valid = $valid;

        return $this;
    }

    /**
     * @return string|null
     */
    public function __toString(): ?string
    {
        return $this->getName();
    }
}