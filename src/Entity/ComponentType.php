<?php


namespace App\Entity;


use App\Model\AbstractTimetrackeableEntity;
use Doctrine\ORM\Mapping as ORM;


/**
 * ComponentType
 *
 * @ORM\Table(name="component_type")
 * @ORM\Entity(repositoryClass="App\Repository\ComponentTypeRepository")
 */
class ComponentType extends AbstractTimetrackeableEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function __toString(): ?string
    {
        return $this->getName();
    }
}