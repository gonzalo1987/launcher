<?php
/**
 * Created by PhpStorm.
 *
 * @author Gonzalo Sáenz
 * User: Usuario
 * Date: 06/05/2019
 * Time: 15:12
 */

namespace App\Entity;


use App\Model\AbstractTimetrackeableEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Phase
 *
 * @ORM\Table(name="phase")
 * @ORM\Entity(repositoryClass="App\Repository\PhaseRepository")
 * @UniqueEntity(
 *      fields={"project", "position"},
 *      errorPath="phase",
 *      message="The position exists for this project."
 * )
 */
class Phase extends AbstractTimetrackeableEntity
{
    /**
     * Phase constructor.
     * @param Project $project
     */
    public function __construct(Project $project)
    {
        parent::__construct();

        $this->setProject($project);
    }

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Project")
     * @ORM\JoinColumn(name="project", referencedColumnName="id")
     */
    private $project;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @var Phase
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Phase")
     * @ORM\JoinColumn(name="parent", referencedColumnName="id", nullable=true)
     */
    private $parent;

    /**
     * @return Project
     */
    public function getProject(): Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     *
     * @return $this
     */
    public function setProject(Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int $position
     *
     * @return $this
     */
    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return Phase|null
     */
    public function getParent(): ?Phase
    {
        return $this->parent;
    }

    /**
     * @param Phase $parent
     *
     * @return $this
     */
    public function setParent(Phase $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return string|null
     */
    public function __toString(): ?string
    {
        return $this->getName();
    }
}