<?php
/**
 * Created by PhpStorm.
 *
 * @author Gonzalo Sáenz
 * User: Usuario
 * Date: 06/05/2019
 * Time: 15:12
 */

namespace App\Entity;


use App\Model\AbstractTimetrackeableEntity;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Step
 *
 * @ORM\Table(name="step")
 * @ORM\Entity(repositoryClass="App\Repository\StepRepository")
 * @UniqueEntity(
 *      fields={"phase", "position"},
 *      errorPath="phase",
 *      message="The position exists for this phase."
 * )
 */
class Step extends AbstractTimetrackeableEntity
{
    public function __construct($phase)
    {
        parent::__construct();

        $this->setPhase($phase);
    }

    /**
     * @var Phase
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Phase", inversedBy="steps")
     * @ORM\JoinColumn(name="phase", referencedColumnName="id")
     */
    private $phase;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @var Step
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Step")
     * @ORM\JoinColumn(name="parent", referencedColumnName="id", nullable=true)
     */
    private $parent;

    /**
     * @var float
     *
     * @ORM\Column(name="fill_required", type="decimal", precision=14, scale=4, nullable=true)
     */
    private $fillRequired;

    /**
     * @var float
     *
     * @ORM\Column(name="reviewed_required", type="decimal", precision=14, scale=4, nullable=true)
     */
    private $reviewedRequired;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="unlocked_at", type="datetime", nullable=true)
     */
    private $unlockedAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="seen", type="datetime", nullable=true)
     */
    private $seen;

    /**
     * @param Phase $phase
     *
     * @return $this
     */
    public function setPhase(Phase $phase): self
    {
        $this->phase = $phase;

        return $this;
    }

    /**
     * @return Phase
     */
    public function getPhase(): Phase
    {
        return $this->phase;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int $position
     *
     * @return $this
     */
    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return Step
     */
    public function getParent(): Step
    {
        return $this->parent;
    }

    /**
     * @param Step $parent
     *
     * @return $this
     */
    public function setParent(Step $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return float
     */
    public function getFillRequired(): float
    {
        return $this->fillRequired;
    }

    /**
     * @param float $fillRequired
     *
     * @return $this
     */
    public function setFillRequired(float $fillRequired): self
    {
        $this->fillRequired = $fillRequired;

        return $this;
    }

    /**
     * @return float
     */
    public function getReviewedRequired(): float
    {
        return $this->reviewedRequired;
    }

    /**
     * @param float $reviewedRequired
     *
     * @return $this
     */
    public function setReviewedRequired(float $reviewedRequired): self
    {
        $this->reviewedRequired = $reviewedRequired;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUnlockedAt(): DateTime
    {
        return $this->unlockedAt;
    }

    /**
     * @param DateTime $unlockedAt
     *
     * @return $this
     */
    public function setUnlockedAt(DateTime $unlockedAt): self
    {
        $this->unlockedAt = $unlockedAt;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getSeen(): DateTime
    {
        return $this->seen;
    }

    /**
     * @param DateTime $seen
     *
     * @return $this
     */
    public function setSeen(DateTime $seen): self
    {
        $this->seen = $seen;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getName();
    }
}