<?php


namespace App\Entity\User;


use App\Model\EntityInterface;
use App\Util\Uuid;
use Doctrine\ORM\Mapping as ORM;
use Sonata\UserBundle\Entity\BaseGroup as BaseGroup;


/**
 * Group
 *
 * @ORM\Table("groups")
 * @ORM\Entity(repositoryClass="App\Repository\GroupRepository")
 */
class Group extends BaseGroup implements EntityInterface
{
    /**
     * Group constructor.
     *
     * @param string $name
     * @param array $roles
     */
    public function __construct($name, $roles = [])
    {
        parent::__construct($name, $roles);

        $this->id = Uuid::uuid();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="string", length=255)
     * @ORM\Id
     */
    protected $id;

    /**
     * Get id.
     *
     * @return string $id
     */
    public function getId(): string
    {
        return $this->id;
    }
}
