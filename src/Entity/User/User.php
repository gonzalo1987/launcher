<?php

namespace App\Entity\User;

use App\Model\EntityInterface;
use App\Util\DateTimeExtension;
use App\Util\Uuid;
use Doctrine\ORM\Mapping as ORM;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;

/**
 * User
 *
 * @ORM\Table("users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseUser implements EntityInterface
{
    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->id = Uuid::uuid();
        $ahora = new DateTimeExtension();
        $this->createdAt = $ahora;
        $this->updatedAt = $ahora;
        $this->enabled = true;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=255)
     * @ORM\Id
     */
    protected $id;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $email;

    /**
     * Get id
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}