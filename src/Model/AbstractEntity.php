<?php


namespace App\Model;

use App\Util\Uuid;
use Doctrine\ORM\Mapping as ORM;

abstract class AbstractEntity implements EntityInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=255)
     * @ORM\Id
     */
    protected $id;

    /**
     * Banco constructor.
     */
    public function __construct()
    {
        $this->id = Uuid::uuid();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}