<?php


namespace App\Model;


use App\Util\DateTimeExtension;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

abstract class AbstractTimetrackeableEntity extends AbstractEntity
{
    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * AbstractTimetrackeableEntity constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $now = new DateTimeExtension();
        $this->createdAt = $now;
        $this->updatedAt = $now;
    }

    /**
     * @param DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(DateTime $createdAt): AbstractTimetrackeableEntity
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(Datetime $updatedAt): AbstractTimetrackeableEntity
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }
}