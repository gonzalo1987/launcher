<?php


namespace App\Model;


use Iterator;


/**
 * Class DoublyLinkedList
 * @package App\Model
 */
class DoublyLinkedList implements Iterator
{
    /** @var DoublyLinkedNodeInterface $first */
    private $first;

    /** @var DoublyLinkedNodeInterface $last */
    private $last;

    /** @var DoublyLinkedNodeInterface $node */
    private $node;

    /** @var int $position */
    private $position;

    /**
     * @param DoublyLinkedNodeInterface $first
     * @param DoublyLinkedNodeInterface $last
     */
    public function __construct(DoublyLinkedNodeInterface $first = null, DoublyLinkedNodeInterface $last = null)
    {
        $this->first = $first;
        if ($first && !$this->last) {
            foreach ($this as $node) {
                if (!$node->getNext()) {
                    $this->last = $node;
                }
            }
        } else {
            $this->last = $last;
        }

        $this->node = $first;
        $this->position = 0;
    }

    /**
     * @param DoublyLinkedNodeInterface $node
     * @param DoublyLinkedNodeInterface $newNode
     *
     * @return $this
     */
    public function insertAfter(DoublyLinkedNodeInterface $node, DoublyLinkedNodeInterface $newNode): self
    {
        $newNode->setPrevious($node);
        if ($node->getNext()) {
            $newNode->setNext($node->getNext());
            $node->getNext()->setPrevious($newNode);
        } else {
            $this->last = $newNode;
        }

        $node->setNext($newNode);

        return $this;
    }

    /**
     * @param DoublyLinkedNodeInterface $node
     * @param DoublyLinkedNodeInterface $newNode
     *
     * @return $this
     */
    public function insertBefore(DoublyLinkedNodeInterface $node, DoublyLinkedNodeInterface $newNode): self
    {
        $newNode->setNext($node);
        if ($node->getPrevious()) {
            $newNode->setPrevious($node->getPrevious());
            $node->getPrevious()->setNext($newNode);
        } else {
            $this->first = $newNode;
        }

        $node->setPrevious($newNode);

        return $this;
    }

    /**
     * @param DoublyLinkedNodeInterface $node
     *
     * @return DoublyLinkedList
     */
    public function removeNode(DoublyLinkedNodeInterface $node): self
    {
        if ($node->getPrevious()) {
            $node->getPrevious()->setNext($node->getNext());
        } else {
            $this->first = $node->getNext();
        }

        if ($node->getNext()) {
            $node->getNext()->setPrevious($node->getPrevious());
        } else {
            $this->last = $node->getPrevious();
        }

        return $this;
    }

    /**
     * @return DoublyLinkedNodeInterface|null
     */
    public function getFirst(): ?DoublyLinkedNodeInterface
    {
        return $this->first;
    }

    /**
     * @return DoublyLinkedNodeInterface
     */
    public function getLast(): ?DoublyLinkedNodeInterface
    {
        return $this->last;
    }

    /**
     * Return the current element
     * @link https://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current(): ?DoublyLinkedNodeInterface
    {
        return $this->node;
    }

    /**
     * Move forward to next element
     * @link https://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        if ($this->node) {
            $this->node = $this->node->getNext();
            $this->position++;
        }
    }

    /**
     * Return the key of the current element
     * @link https://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key(): int
    {
        return $this->position;
    }

    /**
     * Checks if current position is valid
     * @link https://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid(): bool
    {
        return ($this->node) ? true : false;
    }

    /**
     * Rewind the Iterator to the first element
     * @link https://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        $this->node = $this->first;
        $this->position = 0;
    }
}