<?php


namespace App\Model;

/**
 * Interface DoublyLinkedNodeInterface
 * @package App\Model
 */
interface DoublyLinkedNodeInterface
{
    /**
     * @return DoublyLinkedNodeInterface|null
     */
    public function getNext(): ?DoublyLinkedNodeInterface;

    /**
     * @param DoublyLinkedNodeInterface|null $node
     *
     * @return $this
     */
    public function setNext(?DoublyLinkedNodeInterface $node): self;

    /**
     * @return DoublyLinkedNodeInterface|null
     */
    public function getPrevious(): ?DoublyLinkedNodeInterface;

    /**
     * @param DoublyLinkedNodeInterface|null $node
     *
     * @return $this
     */
    public function setPrevious(?DoublyLinkedNodeInterface $node): self;
}