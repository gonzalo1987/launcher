<?php


namespace App\Model;


use Iterator;


/**
 * Class LinkedList
 * @package App\Model
 */
class LinkedList implements Iterator
{
    /** @var LinkedNodeInterface $first */
    private $first;

    /** @var LinkedNodeInterface $node */
    private $node;

    /** @var int $position */
    private $position;

    /**
     * @param LinkedNodeInterface $first
     */
    public function __construct(LinkedNodeInterface $first = null)
    {
        $this->first = $first;
        $this->node = $first;
        $this->position = 0;
    }

    /**
     * @param LinkedNodeInterface $node
     * @param LinkedNodeInterface $newNode
     *
     * @return $this
     */
    public function insertAfter(LinkedNodeInterface $node, LinkedNodeInterface $newNode): self
    {
        if ($node->getNext()) {
            $newNode->setNext($node->getNext());
        }

        $node->setNext($newNode);

        return $this;
    }

    /**
     * @param LinkedNodeInterface $node
     *
     * @return $this
     */
    public function removeNode(LinkedNodeInterface $node): self
    {
        if ($this->first === $node) {
            $this->first = $node->getNext();
        } else {
            $previews = false;
            foreach ($this as $nl) {
                if ($nl->getNext() === $node) {
                    $previews = $nl;
                    break;
                }
            }
            if ($previews) {
                $previews->setNext($node->getNext());
            }
        }

        return $this;
    }

    /**
     * @return LinkedNodeInterface|null
     */
    public function getFirst(): ?LinkedNodeInterface
    {
        return $this->first;
    }

    /**
     * Return the current element
     * @link https://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current(): ?LinkedNodeInterface
    {
        return $this->node;
    }

    /**
     * Move forward to next element
     * @link https://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        if ($this->node) {
            $this->node = $this->node->getNext();
            $this->position++;
        }
    }

    /**
     * Return the key of the current element
     * @link https://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key(): int
    {
        return $this->position;
    }

    /**
     * Checks if current position is valid
     * @link https://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid(): bool
    {
        return ($this->node) ? true : false;
    }

    /**
     * Rewind the Iterator to the first element
     * @link https://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        $this->node = $this->first;
        $this->position = 0;
    }
}