<?php


namespace App\Model;


interface LinkedNodeInterface
{
    /**
     * @return LinkedNodeInterface|null
     */
    public function getNext(): ?LinkedNodeInterface;

    /**
     * @param LinkedNodeInterface|null $node
     *
     * @return $this
     */
    public function setNext(?LinkedNodeInterface $node): self;
}