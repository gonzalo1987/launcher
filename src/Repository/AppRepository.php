<?php


namespace App\Repository;


use App\Util\Repository\Filter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

/**
 * Class AppRepository
 *
 * @package App\Repository
 */
abstract class AppRepository extends ServiceEntityRepository
{
    protected $alias = 'entidad';

    /**
     * Get SQL from query
     *
     * @param Query $query
     *
     * @return string
     * @author Yosef Kaminskyi
     *
     * @see https://stackoverflow.com/questions/2095394/doctrine-how-to-print-out-the-real-sql-not-just-the-prepared-statement/23545733
     *
     */
    public function getFullSQL(Query $query): string
    {
        $sql = $query->getSql();
        $paramsList = $this->getListParamsByDql($query->getDql());
        $paramsArr = $this->getParamsArray($query->getParameters());
        $fullSql = '';
        for ($i = 0; $i < strlen($sql); $i++) {
            if ($sql[$i] == '?') {
                $nameParam = array_shift($paramsList);

                if (is_string($paramsArr[$nameParam])) {
                    $fullSql .= '"' . addslashes($paramsArr[$nameParam]) . '"';
                } elseif (is_array($paramsArr[$nameParam])) {
                    $sqlArr = '';
                    foreach ($paramsArr[$nameParam] as $var) {
                        if (!empty($sqlArr)) {
                            $sqlArr .= ',';
                        }

                        if (is_string($var)) {
                            $sqlArr .= '"' . addslashes($var) . '"';
                        } else
                            $sqlArr .= $var;
                    }
                    $fullSql .= $sqlArr;
                } elseif (is_object($paramsArr[$nameParam])) {
                    switch (get_class($paramsArr[$nameParam])) {
                        case 'DateTime':
                            $fullSql .= "'" . $paramsArr[$nameParam]->format('Y-m-d H:i:s') . "'";
                            break;
                        default:
                            $fullSql .= $paramsArr[$nameParam]->getId();
                    }

                } else
                    $fullSql .= $paramsArr[$nameParam];

            } else {
                $fullSql .= $sql[$i];
            }
        }
        return $fullSql;
    }

    /**
     * Obtiene la lista de parámetros
     *
     * @param ArrayCollection $paramObj
     *
     * @return array
     */
    private function getParamsArray(ArrayCollection $paramObj): array
    {
        $parameters = [];
        foreach ($paramObj as $val) {
            $parameters[$val->getName()] = $val->getValue();
        }

        return $parameters;
    }

    /**
     * @param string $dql
     *
     * @return array
     */
    public function getListParamsByDql(string $dql): array
    {
        $parsedDql = preg_split("/:/", $dql);
        $length = count($parsedDql);
        $parmeters = [];
        for ($i = 1; $i < $length; $i++) {
            if (ctype_alpha($parsedDql[$i][0])) {
                $param = (preg_split("/[' )]/", $parsedDql[$i]));
                $parmeters[] = $param[0];
            }
        }

        return $parmeters;
    }

    /**
     * @param Filter[] $filters
     *
     * @return QueryBuilder
     */
    public function getQueryBuilder($filters = [])
    {
        $qb = $this->createQueryBuilder($this->alias);

        foreach ($filters as $filtro) {
            $filtro->aplicar($qb);
        }

        return $qb;
    }
}