<?php

namespace App\Repository;

use App\Entity\ComponentType;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ComponentType|null find($id, $lockMode = null, $lockVersion = null)
 * @method ComponentType|null findOneBy(array $criteria, array $orderBy = null)
 * @method ComponentType[]    findAll()
 * @method ComponentType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComponentTypeRepository extends SortableRepository
{
    protected $alias = 'component_type';

    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ComponentType::class);
    }
}
