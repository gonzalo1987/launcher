<?php


namespace App\Repository;


use App\Entity\Phase;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Phase|null find($id, $lockMode = null, $lockVersion = null)
 * @method Phase|null findOneBy(array $criteria, array $orderBy = null)
 * @method Phase[]    findAll()
 * @method Phase[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhaseRepository extends SortableRepository
{
    protected $alias = 'phase';

    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Phase::class);
    }
}
