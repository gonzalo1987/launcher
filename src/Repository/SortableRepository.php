<?php


namespace App\Repository;


use App\Util\Repository\Filter;
use Doctrine\ORM\QueryBuilder;

abstract class SortableRepository extends AppRepository
{
    /**
     * @param Filter[] $filters
     * @param array $sorting
     *
     * @return QueryBuilder
     */
    public function getQueryBuilder($filters = [], $sorting = [])
    {
        $qb = parent::getQueryBuilder($filters);

        $this->setQueryBuilderSorting($qb, $sorting);

        return $qb;
    }

    /**
     * @param QueryBuilder $qb
     * @param array $sorting
     */
    protected function setQueryBuilderSorting(QueryBuilder $qb, array $sorting)
    {
        if ($sorting) {
            $fields = array_keys($this->getClassMetadata()->fieldNames);
            foreach ($fields as $field) {
                if (isset($sorting[$field])) {
                    $direction = (mb_strtolower($sorting[$field]) === 'asc') ? 'asc' : 'desc';
                    $field = $this->getClassMetadata()->getFieldName($field);
                    $qb->addOrderBy($this->alias . '.' . $field, $direction);
                }
            }
        }
    }
}