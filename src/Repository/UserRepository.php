<?php


namespace App\Repository;


use App\Entity\User\User;
use Doctrine\Common\Persistence\ManagerRegistry;


/**
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends SortableRepository
{
    protected $alias = 'user';

    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Obtiene el usuario con el id especificado.
     *
     * @param string $id
     * @param int|null $lockMode    One of the \Doctrine\DBAL\LockMode::* constants
     *                              or NULL if no specific lock mode should be used
     *                              during the search.
     * @param int|null $lockVersion The lock version.
     *
     * @return User|null
     */
    public function find($id, $lockMode = null, $lockVersion = null)
    {
        /** @var User $user */
        $user = $this->findOneBy(['id' => $id, 'enabled' => true]);

        return $user;
    }

    /**
     * Obtiene el usuario desactivado con el id especificado.
     *
     * @param string $id
     *
     * @return User|null
     */
    public function findInactiveUser($id)
    {
        /** @var User $user */
        $user = $this->findOneBy(['id' => $id, 'enabled' => false]);

        return $user;
    }

    /**
     * @param string $username
     *
     * @return User|null
     */
    public function findByUsername($username)
    {
        /** @var User $user */
        $user = $this->findOneBy(['username' => $username]);

        return $user;
    }
}
