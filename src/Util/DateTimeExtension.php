<?php
/**
 * Created by PhpStorm.
 *
 * @author Gonzalo Sáenz
 * User: Equipo
 * Date: 10/01/2019
 * Time: 16:55
 */

namespace App\Util;


use DateTime;
use DateTimeZone;
use Exception;


/**
 * Class DateTimeExtension Clase que añade funcionalidad a la clase DateTime
 *
 * @package App\Util
 */
class DateTimeExtension extends Datetime
{
    /**
     * Obtiene la fecha que se pasa como parámetro.
     * Nota: Utilizamos este método cuando requerimos una fecha para controlar
     *     la excepción que podría dar el constructor de \DateTime.
     *
     * @param string $fecha
     * @param DateTimeZone $timezone
     */
    public function __construct($fecha = 'now', DateTimeZone $timezone = null)
    {
        try {
            parent::__construct($fecha, $timezone);
        } catch (Exception $e) {
            (new ExceptionHandler($e))->handle(false);
        }
    }
}