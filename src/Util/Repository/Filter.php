<?php


namespace App\Util\Repository;


use Doctrine\ORM\QueryBuilder;

/**
 * Class Filter
 *
 * @package App\Util
 */
class Filter
{
    /**
     * @param string $field
     * @param string|array $parameters
     * @param int $operation
     */
    public function __construct($field, $parameters = [], $operation = Operation::EQUAL)
    {
        $this->setField($field);
        if (is_array($parameters)) {
            $this->setParameters($parameters);
        } else {
            $this->setParameters([$parameters]);
        }
        $this->operation = $operation;
    }

    /** @var int $operation */
    private $operation = Operation::EQUAL;

    /** @var array $parameters */
    private $parameters = [];

    /** @var bool $null */
    private $null = false;

    /** @var string $alias */
    private $alias = false;

    /** @var string $field */
    private $field;

    /**
     * @return string
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * @param string $operation
     *
     * @return Filter
     */
    public function setOperation($operation): Filter
    {
        $this->operation = $operation;

        return $this;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     *
     * @return Filter
     */
    public function setParameters(array $parameters): Filter
    {
        $arrParametros = [];
        foreach ($parameters as $parametro) {
            if ($parametro !== 'null') {
                $arrParametros[] = $parametro;
            } else {
                $this->null = true;
            }
        }
        $this->parameters = $arrParametros;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     *
     * @return Filter
     */
    public function setAlias($alias): Filter
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     *
     * @return Filter
     */
    public function setField($field)
    {
        $arrCampo = explode('.', $field);
        if (sizeof($arrCampo) === 1) {
            $this->field = $field;
        } else {
            $this->alias = $arrCampo[0];
            $this->field = $arrCampo[1];
        }

        return $this;
    }

    /**
     * @param QueryBuilder $qb
     */
    public function aplicar(QueryBuilder &$qb)
    {
        $alias = ($this->alias) ? $this->alias : $qb->getRootAliases()[0];
        $parametros = $this->parameters;
        $parameterName = $this->field . $this->operation;

        switch ($this->operation) {
            case Operation::EQUAL:
                if ($this->null) {
                    $qb->andWhere($alias . '.' . $this->field . ' is null');
                } else {
                    $qb->andWhere($alias . '.' . $this->field . '=:' . $parameterName);
                }
                break;
            case Operation::IN:
                if ($this->null) {
                    if ($parametros) {
                        $qb->andWhere($alias . '.' . $this->field . ' is null or ' . $alias . '.' . $this->field . ' in (:' . $parameterName . ')');
                    } else {
                        $qb->andWhere($alias . '.' . $this->field . ' is null ');
                    }
                } else {
                    $qb->andWhere($alias . '.' . $this->field . ' in (:' . $parameterName . ')');
                }
                break;
            case Operation::LIKE:
                $qb->andWhere($alias . '.' . $this->field . ' like :' . $parameterName);
                break;
            case Operation::CONTAINS:
                $qb->innerJoin($alias . '.' . $this->field, $this->field, 'with', $this->field . '.id in (:' . $parameterName . ')');
                break;
            case Operation::GREATER_OR_EQUALS_THAN:
                $qb->andWhere($alias . '.' . $this->field . ' >= :' . $parameterName);
                break;
            case Operation::LESS_OR_EQUALS_THAN:
                $qb->andWhere($alias . '.' . $this->field . ' <= :' . $parameterName);
                break;
            case Operation::BETWEEN:
                $qb->andWhere($alias . '.' . $this->field . ' BETWEEN :' . $parameterName . 'start AND :' . $parameterName . 'end');
                $qb->setParameter($parameterName . 'start', $parametros[0]);
                $qb->setParameter($parameterName . 'end', $parametros[1]);
                $parametros = [];
                break;
            case Operation::INNER_JOIN:
                $qb->innerJoin($alias . '.' . $this->field, $this->field);
                break;
            case Operation::LEFT_JOIN:
                $qb->leftJoin($alias . '.' . $this->field, $this->field);
                break;
            case Operation:: OR:
                $where = [];
                /** @var Filter $parametro */
                foreach ($parametros as $parametro) {
                    $where[] = $parametro->obtenerWhere($qb);
                }
                $qb->andWhere(implode(' or ', $where));
                $parametros = false;
                break;
        }
        if ($parametros) {
            $qb->setParameter($parameterName, $parametros);
        }
    }

    /**
     * @param QueryBuilder $qb
     *
     * @return string
     */
    private function obtenerWhere(QueryBuilder &$qb)
    {
        $where = '';
        $alias = ($this->alias) ? $this->alias : $qb->getRootAliases()[0];
        $parametros = $this->parameters;
        $parameterName = $this->field . $this->operation;

        switch ($this->operation) {
            case Operation::EQUAL:
                if ($this->null) {
                    $where = $alias . '.' . $this->field . ' is null';
                } else {
                    $where = $alias . '.' . $this->field . '=:' . $parameterName;
                }
                break;
            case Operation::IN:
                if ($this->null) {
                    $where = $alias . '.' . $this->field . ' is null or ' . $alias . '.' . $this->field . ' in (:' . $parameterName . ')';
                } else {
                    $where = $alias . '.' . $this->field . ' in (:' . $parameterName . ')';
                }
                break;
            case Operation::LIKE:
                $where = $alias . '.' . $this->field . ' like :' . $parameterName;
                break;
            case Operation::GREATER_OR_EQUALS_THAN:
                $where = $alias . '.' . $this->field . ' >= :' . $parameterName;
                break;
            case Operation::LESS_OR_EQUALS_THAN:
                $where = $alias . '.' . $this->field . ' <= :' . $parameterName;
                break;
            case Operation::BETWEEN:
                $where = $alias . '.' . $this->field . ' BETWEEN :' . $parameterName . '-start AND :' . $parameterName . '-end';
                $qb->setParameter($parameterName . 'start', $parametros[0]);
                $qb->setParameter($parameterName . 'end', $parametros[1]);
                $parametros = [];
                break;
        }
        if ($parametros) {
            $qb->setParameter($parameterName, $parametros);
        }

        return $where;
    }
}