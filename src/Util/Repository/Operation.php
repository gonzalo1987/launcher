<?php


namespace App\Util\Repository;


class Operation
{
    public const EQUAL = 1;
    public const IN = 2;
    public const LIKE = 3;
    public const CONTAINS = 4;
    public const GREATER_OR_EQUALS_THAN = 5;
    public const LESS_OR_EQUALS_THAN = 6;
    public const BETWEEN = 7;
    public const INNER_JOIN = 8;
    public const LEFT_JOIN = 9;
    public const OR = 10;
}