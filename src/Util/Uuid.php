<?php


namespace App\Util;


use Exception;

/**
 * Class Uuid
 *
 * @package App\Util
 */
class Uuid
{
    /**
     * Generate Uuid
     *
     * @return int|string
     */
    public static function uuid(): string
    {
        try {
            return \Ramsey\Uuid\Uuid::uuid4()->toString();
        } catch (Exception $e) {
            (new ExceptionHandler($e))->handle();
            return null;
        }
    }
}